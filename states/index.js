import { selector } from "recoil";

import InfoService from "../src/service/InfoService";

export const infoState = selector({
  key: "getInfoSelector",
  get: async () => {
    try {
      const res = await InfoService.getListAll();

      return res;
    } catch (error) {
      console.log(error);
    }
  },
});
