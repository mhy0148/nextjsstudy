import Layout from "../src/component/layout/Layout";

import { dehydrate } from "react-query/hydration";
import { QueryClient } from "react-query";

import ProgramService from "../src/service/ProgramService";
import InfoService from "../src/service/InfoService";

import List from "../src/component/program/List";

const Home = () => {
  return <List />;
};

Home.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};

export const getServerSideProps = async () => {
  const queryClient = new QueryClient();

  await queryClient.prefetchInfiniteQuery(
    ["program-list"],
    () => ProgramService.getList(""),
    { staleTime: 50000 }
  );
  await queryClient.prefetchQuery(["info"], () => InfoService.getListAll(), {
    staleTime: 50000,
  });

  return {
    props: {
      dehydratedState: JSON.parse(JSON.stringify(dehydrate(queryClient))),
    },
  };
};

export default Home;
