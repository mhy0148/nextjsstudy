import React from "react";

import Layout from "../../src/component/layout/Layout";
import ProgramService from "../../src/service/ProgramService";

import Image from "next/image";

const ProgramDetail = ({ data }) => {
  return (
    <div style={{ padding: "10px 5px", textAlign: "center" }}>
      <Image
        src={(data?.images[0] && data?.images[0]?.url) || ""}
        layout="fill"
        alt=""
      />
      <h3>{data?.title || ""}</h3>
    </div>
  );
};

ProgramDetail.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};

export const getServerSideProps = async (context) => {
  const { id } = context.params;

  const res = await ProgramService.info(id);

  return {
    props: {
      data: JSON.parse(JSON.stringify(res)),
    },
  };
};

export default ProgramDetail;
