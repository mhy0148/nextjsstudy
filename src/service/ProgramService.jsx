import { API_URL as API } from "../constants";
import axiosInstance from "../constants/AxiosInstance";

const limit = 10;
const getList = (cursor) => {
  return new Promise((resolve, reject) => {
    const _cursor = cursor ? cursor : "";

    axiosInstance
      .get(`${API}/program?limit=${limit}&cursor=${_cursor}`)
      .then((response) => {
        if (response.data && response.data.value) {
          const val = response.data.value.sort((a, b) =>
            a.createdAt > b.createdAt ? -1 : 1
          );
          resolve({
            list: val,
            total:
              response.data.count > 0
                ? response.data.count
                : response.data.total,
            cursor: response.data.cursor,
            backCursor: response.data.backCusrsor,
          });
        }
      });
  });
};

const getListAt = (date) => {
  return new Promise((resolve, reject) => {
    axiosInstance.get(`${API}/program/filter/${date}`).then((response) => {
      if (response.data && response.data.value) {
        const val = response.data.value.sort((a, b) =>
          a.createdAt > b.createdAt ? -1 : 1
        );
        resolve({
          list: val,
          total:
            response.data.count > 0 ? response.data.count : response.data.total,
          cursor: response.data.cursor,
          backCursor: response.data.backCusrsor,
        });
      }
    });
  });
};

const info = (id) => {
  return new Promise((resolve, reject) => {
    axiosInstance.get(`${API}/program/${id}`).then((response) => {
      if (response?.data?.code === 200) {
        resolve(response?.data?.value);
      }
    });
  });
};

const getMainList = () => {
  return new Promise((resolve, reject) => {
    axiosInstance.get(`${API}/program`).then((response) => {
      if (response.data && response.data.value) {
        const val = response.data.value.sort((a, b) =>
          a.createdAt > b.createdAt ? -1 : 1
        );

        resolve(val);
      }
    });
  });
};

const ProgramService = {
  getMainList,
  getListAt,
  getList,
  info,
};

export default ProgramService;
