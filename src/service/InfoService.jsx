import axiosInstance from "../constants/AxiosInstance";
import { API_URL } from "../constants";

const API = API_URL;

const getListAll = () => {
  return new Promise((resolve, reject) => {
    axiosInstance.get(`${API}/company/info/`).then((response) => {
      if (response.data && response.data.value) {
        const id = new Set();
        const obeject = {};

        response.data.value.map((item) => {
          id.add(item?.id);
        });

        Array.from(id)?.map((item) => {
          obeject[item] = {};
        });

        response.data.value.map((item) => {
          if (obeject[item?.id]) obeject[item?.id] = item;
        });

        resolve(obeject);
      }
    });
  });
};

const info = (id) => {
  return new Promise((resolve, reject) => {
    axiosInstance.get(`${API}/company/info/${id}`).then((response) => {
      if (response.data && response.data.value) {
        resolve(response.data.value);
      }
    });
  });
};

const InfoService = {
  getListAll,
  info,
};

export default InfoService;
