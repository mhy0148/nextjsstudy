import styled, { css } from "styled-components";

import { pd_w } from "./VariablesStyles";

const TOPBAR_HEIGHT = "70px";
const MENU_HEIGHT = "50px";

export const HeaderContainer = styled.header`
  width: 100%;
  position: fixed;
  top: 0;

  padding: ${pd_w};
  background-color: #ffffff;

  ${(props) =>
    props.hide &&
    css`
      display: none;
    `}
`;

export const TopbarContainer = styled.article`
  width: 100%;
  border-bottom: 1px solid #e3e3e3;

  opacity: 0;
  transform: translateY(-120px);
  transition: all 0.3s;

  ${(props) =>
    props.open &&
    css`
      opacity: 1;
      transform: translateY(0px);
    `}

  .web-topbar {
    display: flex;
    justify-content: space-between;
    align-items: center;

    width: 100%;
    height: ${TOPBAR_HEIGHT};

    padding-top: 30px;
  }

  .web-menubar {
    display: flex;
    justify-content: flex-end;
    align-items: center;

    width: 100%;
    height: ${MENU_HEIGHT};
  }
`;

export const AccountContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  button {
    color: #555555;
    font-size: 0.9em;

    &:hover {
      opacity: 0.8;
    }
  }

  .slash {
    width: 1px;
    height: 10px;
    background-color: #eeeeee;
    display: block;
    margin: 0 15px;
  }
`;

export const MainContainer = styled.main`
  width: 100%;
  height: calc(100vh - ${TOPBAR_HEIGHT} + ${MENU_HEIGHT});
  margin-top: calc(${TOPBAR_HEIGHT} + ${MENU_HEIGHT});
  box-sizing: border-box;
  padding: ${pd_w};
`;

export const FooterContainer = styled.footer`
  width: 100%;
  height: 300px;
  background-color: #131c2e;
  color: #ffffff;
  text-align: center;

  padding: 12px;
`;

export const LogoContainer = styled.nav`
  img {
    width: 146px;
    height: 43px;
    object-fit: fill;
  }
`;

/* ////////////////////////////////////////////
                    Menu
//////////////////////////////////////////// */
export const MenuContainer = styled.ul`
  display: flex;
  justify-content: center;
  align-items: center;
  gap: 40px;

  .menu-item-container {
    position: relative;
    color: #555555;
    font-size: 0.9em;

    cursor: pointer;
    letter-spacing: -0.28px;

    &:hover {
      & > p {
        color: ${(props) => props.theme.primary};
        font-weight: bold;
      }
    }

    &.focus {
      color: ${(props) => props.theme.primary} !important;
    }
  }

  .menu-item-container.active {
    &::after {
      content: "";
      position: absolute;
      width: 100%;
      height: 300%;
      top: 0;
    }

    .menu-collapse-background {
      width: 100vw;
      height: 100vh;

      position: fixed;
      top: 0;
      left: 0;
    }
  }

  .menu-children-container {
    width: 84px;

    position: absolute;
    top: 40px;
    left: 50%;

    border-radius: 2px;
    transform: translateX(-50%);
    transition: opacity 0.3s;
    padding: 5px;

    background-color: #ffffff;
    opacity: 0.94;
    box-shadow: 1px 1px 6px -1px rgb(0 0 0 / 30%);

    &.un {
      opacity: 0;
      pointer-events: none;
    }

    &.actice {
      opacity: 1;
      pointer-events: auto;
    }
  }

  .menu-point {
    color: rgba(255, 255, 255, 0.3);

    position: absolute;
    top: -10px;
    left: 40%;

    ${(props) =>
      props.colorAdd &&
      css`
        color: rgba(0, 0, 0, 0.06);
      `}
  }

  .menu-item-children-container {
    color: #555555;
    text-align: center;
    padding: 6px 12px;
    border-bottom: 1px solid #eeeeee;

    &:hover {
      background-color: rgba(0, 0, 0, 0.03);
      color: #555555;
    }

    &:last-child {
      border-bottom: none;
    }
  }
`;

export const MenuAccodion = styled.ul`
  width: 100%;
  background-color: #ffffff;
  overflow: hidden;

  .accodion-item {
    width: 100%;
    height: 45px;
    display: flex;
    align-items: center;
    padding: 0 5%;

    color: #555555;
    font-size: 0.95em;
    font-weight: bold;

    cursor: pointer;

    &.focus {
      background-color: ${(props) => props.theme.primary};
      color: #ffffff;
    }
  }

  .accodion-children-container {
    max-height: 0px;
    padding: 0 5%;
    overflow: hidden;

    transition: max-height 0.65s ease-out;

    &.active {
      max-height: 500px;
      transition: max-height 0.65s ease-in;
    }
  }

  .accodion-children-item {
    width: 100%;
    height: 45px;
    display: flex;
    align-items: center;

    color: #868686;
    font-size: 0.95em;
    font-weight: bold;

    border-bottom: 1px solid #eeeeee;

    cursor: pointer;
  }
`;
