import styled from "styled-components";

export const MainListContainer = styled.div`
  width: 100%;

  padding-top: 50px;

  ul {
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    gap: 40px;
  }

  .list-item {
    width: 100%;

    img {
      width: 100%;
      height: 250px;
    }
  }

  p {
    color: #555555;
    font-size: 0.9em;
  }
`;
