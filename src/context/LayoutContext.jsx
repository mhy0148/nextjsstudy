import React, { useState, useEffect } from "react";

import { MOBILE_SIZE } from "../styled/VariablesStyles";

export const LayoutContext = React.createContext({
  matchese:
    typeof window === "object"
      ? window.matchMedia(`(max-width:${MOBILE_SIZE})`).matches
      : null,
});

const LayoutContextProvider = (props) => {
  const [matchese, setMatches] = useState(false);

  useEffect(() => {
    resizePage();
  }, []);

  useEffect(() => {
    window.addEventListener("resize", resizePage);

    return () => window.addEventListener("resize", resizePage);
  }, []);

  const resizePage = (e) => {
    if (typeof window !== undefined) {
      setMatches(window.matchMedia(`(max-width:${MOBILE_SIZE})`).matches);
    }
  };

  return (
    <LayoutContext.Provider
      value={{
        matchese: matchese,
      }}
    >
      {props.children}
    </LayoutContext.Provider>
  );
};

export default LayoutContextProvider;
