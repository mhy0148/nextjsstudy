const alert = async (header, text, cancelButtonText, confirmButtonText, _select) => {
  let select = _select;

  const _alertCreate = async (header, text, cancelButtonText, confirmButtonText) => {
    let button = `<button id="customBtnSelect"><p>확인</p></button>`;

    if (cancelButtonText && confirmButtonText) {
      button = `
                <button id="customBtnSelect"><p>${confirmButtonText}</p></button>
                <button id="customBtnClose"><p>${cancelButtonText}</p></button>`;
    }

    let ele = document.querySelector('body');
    var _alert = document.createElement('div');
    _alert.className = 'alertBg show-alert';
    _alert.id = 'alertBg';
    _alert.innerHTML = `<div class="custom-alert">
                              <div class="alert-text">
                                  <p>${header}</p>
                                  <p>${text}</p>
                              </div>
                              <div class="alert-footer">
                                 ${button}
                              </div>
                          </div>`;
    ele.appendChild(_alert);

    if (confirmButtonText && document.getElementById('customBtnClose'))
      document.getElementById('customBtnClose').onclick = () => selectAlert(false);

    document.getElementById('customBtnSelect').onclick = () => selectAlert(true);
    document.getElementById('alertBg').onclick = e => currentTargetClick(e);
  };

  const currentTargetClick = ({ target }) => {
    const alert = document.querySelector('.custom-alert');

    if (alert) {
      if (!alert.contains(target)) {
        selectAlert(false);
      }
    }
  };

  const selectAlert = _res => {
    const child = document.getElementById('alertBg');

    if (select) select(_res);
    if (child) child.parentNode.removeChild(child);
  };

  if (!!!document.getElementById('alertBg')) {
    _alertCreate(header, text, cancelButtonText, confirmButtonText);
  }
};

export default alert;

// Ex)

// Alert('잘 둘러보고 계신가요?', '소도시라이프를 더욱 즐기고 싶으시다면 <br /> 로그인 해주세요!', '아니요', '예', (res) => {
//   console.log(res);
// });

// text <b> HighLight </b>

// cancelButtonText : 취소버튼 영역
// confirmButtonText : 확인버튼 영역

// _select 선택한 결과 콜백
