import React, { useState, useEffect } from "react";

import { MenuContainer } from "../../styled/LayoutStyled";

import { MENU } from "./NavigationConfig";
import { VscDebugBreakpointFunction } from "react-icons/vsc";

const Menu = ({ goUrl }) => {
  return (
    <MenuContainer>
      {MENU.map((item, i) => (
        <MenuItem
          label={item?.label || ""}
          type={item?.type || ""}
          anchor={item?.anchor || []}
          childrens={item?.children || []}
          pathname={item?.pathname || ""}
          onClick={goUrl}
          key={`menu-item-${i}`}
        ></MenuItem>
      ))}
    </MenuContainer>
  );
};

export default Menu;

const MenuItem = ({ label, type, anchor, childrens, pathname, onClick }) => {
  const location = null;

  const [focus, setFocus] = useState(false);
  const [active, setAcitve] = useState(false);

  useEffect(() => {
    const now = location?.pathname;

    anchor.map((an) => {
      if (an === now) setAcitve(true);
    });
  }, [anchor]);

  const onFocus = () => {
    if (focus) return;
    if (pathname) return;

    setFocus(true);
  };

  const onBlur = () => {
    setFocus(false);
  };

  return (
    <li
      className={`menu-item-container 
      ${focus ? "active" : "un"} 
      ${active ? "focus" : "un"}`}
      onMouseOver={onFocus}
    >
      <p onClick={() => onClick(pathname)}>{label}</p>
      <div className="menu-collapse-background" onMouseOver={onBlur}></div>

      {type === "collapse" && (
        <ul
          className={`menu-children-container ${focus ? "active" : "un"}`}
          onMouseOver={onFocus}
        >
          <VscDebugBreakpointFunction className="menu-point" />
          {childrens &&
            childrens?.map((cr, i) => (
              <MenuChildren
                label={cr?.label || ""}
                pathname={cr?.pathname || ""}
                onClick={onClick}
                key={`menu-item-children-${i}`}
              />
            ))}
        </ul>
      )}
    </li>
  );
};

const MenuChildren = ({ label, pathname, onClick }) => {
  return (
    <li
      className={`menu-item-children-container`}
      onClick={() => onClick(pathname)}
    >
      <p>{label}</p>
    </li>
  );
};
