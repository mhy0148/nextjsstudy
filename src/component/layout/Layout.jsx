import React, { lazy } from "react";

const Header = lazy(() => import("./Header"));
const Main = lazy(() => import("./Main"));
const Footer = lazy(() => import("./Footer"));

const Layout = ({ children, headerHide }) => {
  return (
    <React.Fragment>
      <Header hide={headerHide} />
      <Main>{children}</Main>
      <Footer />
    </React.Fragment>
  );
};

export default Layout;
