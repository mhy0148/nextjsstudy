import React from "react";

import { MainContainer } from "../../styled/LayoutStyled";

const Main = ({ children }) => {
  return <MainContainer>{children}</MainContainer>;
};

export default Main;
