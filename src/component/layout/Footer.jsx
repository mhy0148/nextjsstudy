import React, { useEffect, useState } from "react";

import { FooterContainer } from "../../styled/LayoutStyled";

import { useQuery } from "react-query";
import InfoService from "../../service/InfoService";

const Footer = () => {
  const { data } = useQuery(["info"], () => InfoService.getListAll());

  return (
    <FooterContainer>
      <p>{data?.company?.ceo}</p>
      <p>{data?.company?.address}</p>
    </FooterContainer>
  );
};

export default Footer;
