import React from "react";

import { ThemeProvider } from "styled-components";
import { createGlobalStyle } from "styled-components";
import { normalize } from "styled-normalize";

// Theme Styled
const GlobalStyle = createGlobalStyle`
    ${normalize}

    body {
      background-color: #ffffff;
    }

  `;

const Theme = ({ children }) => {
  const theme = {
    background: "#ffffff",
    primary: "#f5a302",
    footer: "#131c2e",
    button: "#1d2f70",
  };

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      {children}
    </ThemeProvider>
  );
};

export default Theme;
