export const MENU = [
  {
    label: "회사소개",
    anchor: ["/info/vision", "/info/team"],
    type: "collapse",
    children: [
      {
        label: "비전",
        anchor: "vision",
        type: "item",
        pathname: "/info/vision",
      },
      {
        label: "팀",
        anchor: "team",
        type: "item",
        pathname: "/info/team",
        //
      },
    ],
  },
  {
    label: "프로그램",
    anchor: ["/program", "/master"],
    type: "collapse",
    children: [
      {
        label: "프로그램",
        anchor: "program",
        type: "item",
        pathname: "/program",
      },
      {
        label: "마스터",
        anchor: "master",
        type: "item",
        pathname: "/master",
      },
    ],
  },
  {
    label: "전체 일정",
    anchor: ["/schedule"],
    type: "item",
    pathname: "/schedule",
  },
  {
    label: "소식지",
    anchor: ["/news/recommend", "/news/interview", "/news/event"],
    type: "collapse",
    children: [
      {
        label: "추천지",
        anchor: "recommend",
        type: "item",
        pathname: "/news/recommend",
      },
      {
        label: "인터뷰",
        anchor: "interview",
        type: "item",
        pathname: "/news/interview",
      },
      {
        label: "이벤트",
        anchor: "event",
        type: "item",
        pathname: "/news/event",
      },
    ],
  },
  {
    label: "참여후기",
    anchor: ["/review"],
    type: "item",
    pathname: "/review",
  },
  {
    label: "마이페이지",
    anchor: ["/my/schedule", "/my/order", "/my/info"],
    type: "collapse",
    children: [
      {
        label: "내 일정",
        anchor: "my-schedule",
        type: "item",
        pathname: "/my/schedule",
      },
      {
        label: "주문현황",
        anchor: "my-order",
        type: "item",
        pathname: "/my/order",
      },
      {
        label: "내 정보",
        anchor: "my-info",
        type: "item",
        pathname: "/my/info",
      },
    ],
  },
];

export const FOOTER_MENU = [
  // {
  //   label: "개인정보관리 책임자",
  //   anchor: "info",
  //   pathname: "/info/private",
  // },
  {
    label: "개인정보처리방침",
    anchor: "policy",
    pathname: "/info/policy",
  },
  {
    label: "이용약관",
    anchor: "schedule",
    pathname: "/info/terms",
  },
];
