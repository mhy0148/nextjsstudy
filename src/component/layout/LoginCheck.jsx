import React from "react";

import { loginCheck } from "../../constants";
import { AccountContainer } from "../../styled/LayoutStyled";

const LoginCheck = ({ goUrl }) => {
  return (
    <AccountContainer>
      {loginCheck() ? (
        <>
          <span className="slash" />
          <button onClick={logout}>로그아웃</button>
        </>
      ) : (
        <>
          <button onClick={() => goUrl("/login")}>로그인</button>
          <span className="slash" />
          <button onClick={() => goUrl("/join")}>회원가입</button>
        </>
      )}
    </AccountContainer>
  );
};

export default LoginCheck;
