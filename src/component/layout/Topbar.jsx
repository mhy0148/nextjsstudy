import React, { useContext } from "react";

import { TopbarContainer } from "../../styled/LayoutStyled";

import { LayoutContext } from "../../context/LayoutContext";

import Logo from "../common/Logo";
import Menu from "./Menu";
import LoginCheck from "./LoginCheck";

const Topbar = ({ open }) => {
  const { matchese } = useContext(LayoutContext);

  const goUrl = (url) => {
    console.log("");
  };

  return (
    <TopbarContainer open={open}>
      {matchese ? <MobileTopbar goUrl={goUrl} /> : <WebTopbar goUrl={goUrl} />}
    </TopbarContainer>
  );
};

const WebTopbar = ({ goUrl }) => {
  return (
    <React.Fragment>
      <section className="web-topbar">
        <div className="logo-container">
          <Logo />
        </div>

        <div className="option-container">
          <LoginCheck goUrl={goUrl} />
        </div>
      </section>

      <section className="web-menubar">
        <Menu goUrl={goUrl} />
      </section>
    </React.Fragment>
  );
};

const MobileTopbar = () => {
  return <div />;
};

export default Topbar;
