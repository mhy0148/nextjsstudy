import React, { useRef, useMemo, useState, useEffect } from "react";

import { throttle } from "lodash";

import { HeaderContainer } from "../../styled/LayoutStyled";

import Topbar from "./Topbar";

const Header = ({ hide }) => {
  const headerRef = useRef();

  const [headerOpen, setHeaderOpen] = useState(true);
  const [scrollBtnOpen, setScrollBtnOpen] = useState(false);

  useEffect(() => {
    if (typeof window !== undefined) {
      window.addEventListener("scroll", listener);
      window.addEventListener("scroll", handleScroll);

      return () => {
        window.removeEventListener("scroll", listener);
        window.removeEventListener("scroll", handleScroll);
      };
    }
  }, [scrollBtnOpen]);

  // Scroll Action
  const listener = useMemo(
    () =>
      throttle(() => {
        if (!headerRef.current) return;

        const open = headerRef.current.clientHeight < window.scrollY;
        if (scrollBtnOpen !== open) setScrollBtnOpen(open);
      }, [300]),
    [scrollBtnOpen]
  );

  // Header Open
  const handleScroll = useMemo(() => {
    let ls = 0;

    return throttle(() => {
      if (!headerRef.current) return;

      let st = window.scrollY;
      let height = headerRef.current.clientHeight;

      if (st > ls) {
        setHeaderOpen(false);
      } else if (st < ls) {
        setHeaderOpen(true);
      }

      if (st < height) {
        setHeaderOpen(true);
      }

      ls = st;
    }, 300);
  }, [headerOpen]);

  return (
    <HeaderContainer hide={hide} ref={headerRef}>
      <Topbar open={headerOpen} />
    </HeaderContainer>
  );
};

export default Header;
