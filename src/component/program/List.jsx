import React from "react";

import { useInfiniteQuery } from "react-query";
import ProgramService from "../../service/ProgramService";

import Link from "next/link";
import Image from "next/image";

import { MainListContainer } from "../../styled/MainStyled";

const List = () => {
  const { data, fetchNextPage } = useInfiniteQuery(
    ["program-list"],
    (cursor) => ProgramService.getList(cursor?.pageParam),
    {
      getNextPageParam: (lastPage, pages) => {
        let result;
        if (!lastPage?.isLast) result = lastPage.nextPage;
        else result = undefined;

        return result;
      },
    }
  );

  return (
    <MainListContainer>
      <ul>
        {data &&
          data?.pages?.map((page) =>
            page?.list.map((data) => (
              <li key={data.title} className="list-item">
                <Link href={"/detail/[id]"} as={`/detail/${data?.id}`}>
                  <a>
                    <Image
                      src={data?.images[0] ? data?.images[0]?.url : ""}
                      alt={data?.title || ""}
                      width={100}
                      height={100}
                    />
                    <p>{data?.title || ""}</p>
                  </a>
                </Link>
              </li>
            ))
          )}
      </ul>
    </MainListContainer>
  );
};

export default List;
