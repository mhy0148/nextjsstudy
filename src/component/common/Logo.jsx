import React from "react";

import { IMG_PATH } from "../../constants";

import { LogoContainer } from "../../styled/LayoutStyled";

import Image from "next/image";

const Logo = () => {
  return (
    <LogoContainer>
      <Image src={`${IMG_PATH}/logo.png`} alt="" width={146} height={43} />
    </LogoContainer>
  );
};

export default Logo;
